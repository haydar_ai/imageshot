﻿using Microsoft.Phone.Tasks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace ImageShot.Commands
{
    public class SendAnEmailCommand : ICommand
    {
        public bool CanExecute(object parameter)
        {
            return true;
        }

        public event EventHandler CanExecuteChanged;

        public void Execute(object parameter)
        {
            EmailComposeTask emailTask = new EmailComposeTask();
            emailTask.To = "haydar_ai@yaahoo.com";
            emailTask.Subject = "ImageShot";
            emailTask.Show();
        }
    }
}
