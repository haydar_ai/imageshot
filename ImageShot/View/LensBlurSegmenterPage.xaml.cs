﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Media;
using System.Windows.Shapes;
using Microsoft.Phone.Tasks;
using ImageShot.Model;
using System.Windows.Media.Imaging;
using Nokia.Graphics.Imaging;
using Nokia.InteropServices.WindowsRuntime;

namespace ImageShot.View
{
    public partial class LensBlurSegmenterPage : PhoneApplicationPage
    {
        private SolidColorBrush _brush;
        private Polyline _polyline;
        private bool _processing;
        private bool _processingPending;
        private PhotoResult _photoResult;
        private bool _manipulating;
        private bool _renderedSuccesfully;

        private bool Processing
        {
            get
            {
                return _processing;
            }
            set
            {
                if (_processing != value)
                {
                    _processing = value;
                    progressBar.IsIndeterminate = _processing;
                    progressBar.Visibility = _processing ? Visibility.Visible : Visibility.Collapsed;
                }
            }
        }

        private bool AnnotationsDrawn
        {
            get
            {
                return AnnotationsCanvas.Children.Count > 0;
            }
        }

        private bool ForegroundAnnotationsDrawn
        {
            get
            {
                return AnnotationsCanvas.Children.Cast<Polyline>().Any(p => p.Stroke == Photo.ForegroundBrush);
            }
        }

        private bool BackgroundAnnotationsDrawn
        {
            get
            {
                return AnnotationsCanvas.Children.Cast<Polyline>().Any(p => p.Stroke == Photo.BackgroundBrush);
            }
        }

        public LensBlurSegmenterPage()
        {
            InitializeComponent();

            OriginalImage.LayoutUpdated += OriginalImage_LayoutUpdated;
        }

        private void OriginalImage_LayoutUpdated(object sender, EventArgs e)
        {
            MaskImage.Width = OriginalImage.ActualWidth;
            MaskImage.Height = OriginalImage.ActualHeight;

            AnnotationsCanvas.Width = OriginalImage.ActualWidth;
            AnnotationsCanvas.Height = OriginalImage.ActualHeight;
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            if (Photo.OriginalImage == null)
            {
                NavigationService.GoBack();
            }
            else
            {
                if (_photoResult != null)
                {
                    Photo.OriginalImage = _photoResult.ChosenPhoto;
                    _photoResult = null;

                    AnnotationsCanvas.Children.Clear();

                    Photo.Saved = false;
                }

                if (Photo.OriginalImage != null)
                {
                    if (_brush == null)
                    {
                        _brush = Photo.ForegroundBrush;
                    }

                    var originalBitmap = new BitmapImage
                    {
                        DecodePixelWidth = (int)(480.0 * Application.Current.Host.Content.ScaleFactor / 100.0)
                    };

                    Photo.OriginalImage.Position = 0;

                    originalBitmap.SetSource(Photo.OriginalImage);

                    OriginalImage.Source = originalBitmap;
                    AttemptUpdatePreviewAsync();
                }
                else
                {
                    _brush = null;
                }

                AdaptButtonsToState();

                ManipulationArea.ManipulationStarted += AnnotationsCanvas_ManipulationStarted;
                ManipulationArea.ManipulationDelta += AnnotationsCanvas_ManipulationDelta;
                ManipulationArea.ManipulationCompleted += AnnotationsCanvas_ManipulationCompleted;
            }
        }

        private Point NearestPointInElement(double x, double y, FrameworkElement element)
        {
            var clampedX = Math.Min(Math.Max(0, x), element.ActualWidth);
            var clampedY = Math.Min(Math.Max(0, y), element.ActualHeight);

            return new Point(clampedX, clampedY);
        }

        private void AnnotationsCanvas_ManipulationStarted(object sender, System.Windows.Input.ManipulationStartedEventArgs e)
        {
            _manipulating = true;

            _polyline = new Polyline
            {
                Stroke = _brush,
                StrokeThickness = 6
            };

            var manipulationAreaDeltaX = ManipulationArea.Margin.Left;
            var manipulationAreaDeltaY = ManipulationArea.Margin.Top;

            var point = NearestPointInElement(e.ManipulationOrigin.X + manipulationAreaDeltaX, e.ManipulationOrigin.Y + manipulationAreaDeltaY, AnnotationsCanvas);

            _polyline.Points.Add(point);

            CurrentAnnotationCanvas.Children.Add(_polyline);
        }

        private void AnnotationsCanvas_ManipulationDelta(object sender, System.Windows.Input.ManipulationDeltaEventArgs e)
        {
            var manipulationAreaDeltaX = ManipulationArea.Margin.Left;
            var manipulationAreaDeltaY = ManipulationArea.Margin.Top;

            var x = e.ManipulationOrigin.X - e.DeltaManipulation.Translation.X + manipulationAreaDeltaX;
            var y = e.ManipulationOrigin.Y - e.DeltaManipulation.Translation.Y + manipulationAreaDeltaY;

            var point = NearestPointInElement(x, y, AnnotationsCanvas);

            _polyline.Points.Add(point);
        }

        private void AnnotationsCanvas_ManipulationCompleted(object sender, System.Windows.Input.ManipulationCompletedEventArgs e)
        {
            if (_polyline.Points.Count < 2)
            {
                CurrentAnnotationCanvas.Children.Clear();

                _manipulating = false;
            }
            else
            {
                CurrentAnnotationCanvas.Children.RemoveAt(CurrentAnnotationCanvas.Children.Count - 1);

                AnnotationsCanvas.Children.Add(_polyline);

                Photo.Saved = false;

                AdaptButtonsToState();

                _manipulating = false;

                AttemptUpdatePreviewAsync();
            }

            _polyline = null;
        }

        private async void AttemptUpdatePreviewAsync()
        {
            if (!Processing)
            {
                Processing = true;

                AdaptButtonsToState();

                do
                {
                    _processingPending = false;

                    if (Photo.OriginalImage != null && ForegroundAnnotationsDrawn && BackgroundAnnotationsDrawn)
                    {
                        Photo.OriginalImage.Position = 0;

                        var maskBitmap = new WriteableBitmap((int)AnnotationsCanvas.ActualWidth, (int)AnnotationsCanvas.ActualHeight);
                        var annotationsBitmap = new WriteableBitmap((int)AnnotationsCanvas.ActualWidth, (int)AnnotationsCanvas.ActualHeight);

                        annotationsBitmap.Render(AnnotationsCanvas, new ScaleTransform
                        {
                            ScaleX = 1,
                            ScaleY = 1
                        });

                        annotationsBitmap.Invalidate();

                        Photo.OriginalImage.Position = 0;

                        using (var source = new StreamImageSource(Photo.OriginalImage))
                        using (var segmenter = new InteractiveForegroundSegmenter(source))
                        using (var renderer = new WriteableBitmapRenderer(segmenter, maskBitmap))
                        using (var annotationsSource = new BitmapImageSource(annotationsBitmap.AsBitmap()))
                        {
                            var foregroundColor = Photo.ForegroundBrush.Color;
                            var backgroundColor = Photo.BackgroundBrush.Color;

                            segmenter.ForegroundColor = Windows.UI.Color.FromArgb(foregroundColor.A, foregroundColor.R, foregroundColor.G, foregroundColor.B);
                            segmenter.BackgroundColor = Windows.UI.Color.FromArgb(backgroundColor.A, backgroundColor.R, backgroundColor.G, backgroundColor.B);
                            segmenter.Quality = 0.5;
                            segmenter.AnnotationsSource = annotationsSource;

                            try
                            {
                                await renderer.RenderAsync();

                                MaskImage.Source = maskBitmap;

                                maskBitmap.Invalidate();

                                Photo.AnnotationsBitmap = (Bitmap)annotationsBitmap.AsBitmap();

                                _renderedSuccesfully = true;
                            }
                            catch (Exception ex)
                            {
                                System.Diagnostics.Debug.WriteLine("AttemptUpdatePreviewAsync rendering failed: " + ex.Message);
                                _renderedSuccesfully = false;
                            }
                        }
                    }
                    else
                    {
                        MaskImage.Source = null;
                    }
                }
                while (_processingPending && !_manipulating);

                Processing = false;

                AdaptButtonsToState();
            }
            else
            {
                _processingPending = true;
            }
        }

        private void AdaptButtonsToState()
        {
            // undoButton.IsEnabled = AnnotationsDrawn;
            // doneButton.IsEnabled = ForegroundAnnotationsDrawn && BackgroundAnnotationsDrawn && !Processing && _renderedSuccesfully;
            // resetButton.IsEnabled = AnnotationsDrawn;

            if (Photo.OriginalImage != null)
            {
                ForegroundButton.IsEnabled = true;
                BackgroundButton.IsEnabled = true;

                ForegroundButton.Background = _brush == Photo.ForegroundBrush ? Photo.ForegroundBrush : null;
                BackgroundButton.Background = _brush == Photo.BackgroundBrush ? Photo.BackgroundBrush : null;
            }
            else
            {
                ForegroundButton.IsEnabled = false;
                BackgroundButton.IsEnabled = false;
            }
        }

        private void OnResetClicked(object sender, EventArgs e)
        {
            AnnotationsCanvas.Children.Clear();
            Photo.Saved = false;
            AdaptButtonsToState();
            AttemptUpdatePreviewAsync();
        }

        private void OnUndoClicked(object sender, EventArgs e)
        {
            AnnotationsCanvas.Children.RemoveAt(AnnotationsCanvas.Children.Count - 1);
            Photo.Saved = false;
            AdaptButtonsToState();
            AttemptUpdatePreviewAsync();
        }

        private void OnCheckClicked(object sender, EventArgs e)
        {
            if (!Processing && !_manipulating && Photo.AnnotationsBitmap != null)
            {
                NavigationService.Navigate(new Uri("/View/EffectPage.xaml", UriKind.Relative));
            }
        }

        private void foregroundButtonClicked(object sender, RoutedEventArgs e)
        {
            _brush = Photo.ForegroundBrush;
            AdaptButtonsToState();
        }

        private void backgroundButtonClicked(object sender, RoutedEventArgs e)
        {
            _brush = Photo.BackgroundBrush;
            AdaptButtonsToState();
        }
    }
}