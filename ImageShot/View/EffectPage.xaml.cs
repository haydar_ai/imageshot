﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using ImageShot.Model;
using System.Windows.Media;
using Nokia.Graphics.Imaging;
using System.Windows.Media.Imaging;
using System.Runtime.InteropServices.WindowsRuntime;
using Microsoft.Phone.Info;
using Windows.Storage.Streams;
using Microsoft.Xna.Framework.Media;

namespace ImageShot.View
{
    public partial class EffectPage : PhoneApplicationPage
    {
        private bool _processing;
        private bool _processingPending;

        private bool Processing
        {
            get
            {
                return _processing;
            }
            set
            {
                if (_processing != value)
                {
                    _processing = value;
                    ProgressBar.IsIndeterminate = _processing;
                    ProgressBar.Visibility = _processing ? Visibility.Visible : Visibility.Collapsed;
                }
            }
        }
        public EffectPage()
        {
            InitializeComponent();
            SizeSlider.ValueChanged += SizeSlider_ValueChanged;
            if (Photo.KernelSize > 0.5)
            {
                SizeSlider.Value = Photo.KernelSize;
            }
            else
            {
                Photo.KernelSize = SizeSlider.Value;
            }
        }

        void SizeSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            AttemptUpdatePreviewAsync();

            Photo.Saved = false;
            Photo.KernelSize = e.NewValue;

            AdaptButtonToState();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            if (Photo.OriginalImage == null | Photo.AnnotationsBitmap == null)
            {
                NavigationService.GoBack();
            }
            else
            {
                AdaptButtonToState();
                AttemptUpdatePreviewAsync();
            }
        }

        private void AdaptButtonToState()
        {
            var accentColorBrush = (Brush)Application.Current.Resources["PhoneAccentBrush"];
            var transparentBrush = (Brush)Application.Current.Resources["TransparentBrush"];

            CircleButton.Background = Photo.KernelShape == Nokia.Graphics.Imaging.LensBlurPredefinedKernelShape.Circle ? accentColorBrush : transparentBrush;
            HexagonButton.Background = Photo.KernelShape == Nokia.Graphics.Imaging.LensBlurPredefinedKernelShape.Hexagon ? accentColorBrush : transparentBrush;
            FlowerButton.Background = Photo.KernelShape == Nokia.Graphics.Imaging.LensBlurPredefinedKernelShape.Flower ? accentColorBrush : transparentBrush;
            StarButton.Background = Photo.KernelShape == Nokia.Graphics.Imaging.LensBlurPredefinedKernelShape.Star ? accentColorBrush : transparentBrush;
            HeartButton.Background = Photo.KernelShape == Nokia.Graphics.Imaging.LensBlurPredefinedKernelShape.Heart ? accentColorBrush : transparentBrush;
        }

        private async void AttemptUpdatePreviewAsync()
        {
            if (!Processing)
            {
                Processing = true;
                AdaptButtonToState();
                Photo.OriginalImage.Position = 0;

                using (var source = new StreamImageSource(Photo.OriginalImage))
                using (var segmenter = new InteractiveForegroundSegmenter(source))
                using (var annotationsSource = new BitmapImageSource(Photo.AnnotationsBitmap))
                {
                    segmenter.Quality = 0.5;
                    segmenter.AnnotationsSource = annotationsSource;
                    var foregroundColor = Photo.ForegroundBrush.Color;
                    var backgroundColor = Photo.BackgroundBrush.Color;
                    segmenter.ForegroundColor = Windows.UI.Color.FromArgb(foregroundColor.A, foregroundColor.R, foregroundColor.G, foregroundColor.B);
                    segmenter.BackgroundColor = Windows.UI.Color.FromArgb(backgroundColor.A, backgroundColor.R, backgroundColor.G, backgroundColor.B);

                    do
                    {
                        _processingPending = false;
                        var previewBitmap = new WriteableBitmap((int)Photo.AnnotationsBitmap.Dimensions.Width, (int)Photo.AnnotationsBitmap.Dimensions.Height);
                        using (var effect = new LensBlurEffect(source, new LensBlurPredefinedKernel(Photo.KernelShape, (uint)Photo.KernelSize)))
                        using (var renderer = new WriteableBitmapRenderer(effect, previewBitmap))
                        {
                            effect.KernelMap = segmenter;

                            try
                            {
                                await renderer.RenderAsync();
                                PreviewImage.Source = previewBitmap;
                                previewBitmap.Invalidate();
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("Image too large, please choose a smaller picture");
                                NavigationService.Navigate(new Uri("/MainPage.xaml", UriKind.Relative));
                            }
                        }
                    }
                    while (_processingPending);
                }
                Processing = false;
                AdaptButtonToState();
            }
            else
            {
                _processingPending = true;
            }

        }

        private void CircleButton_Click(object sender, RoutedEventArgs e)
        {
            if (Photo.KernelShape != LensBlurPredefinedKernelShape.Circle)
            {
                Photo.KernelShape = LensBlurPredefinedKernelShape.Circle;
                AttemptUpdatePreviewAsync();
                Photo.Saved = false;
                AdaptButtonToState();
            }
        }

        private void HexagonButton_Click(object sender, RoutedEventArgs e)
        {
            if (Photo.KernelShape != LensBlurPredefinedKernelShape.Hexagon)
            {
                Photo.KernelShape = LensBlurPredefinedKernelShape.Hexagon;
                AttemptUpdatePreviewAsync();
                Photo.Saved = false;
                AdaptButtonToState();
            }
        }

        private void FlowerButton_Click(object sender, RoutedEventArgs e)
        {
            if (Photo.KernelShape != LensBlurPredefinedKernelShape.Flower)
            {
                Photo.KernelShape = LensBlurPredefinedKernelShape.Flower;
                AttemptUpdatePreviewAsync();
                Photo.Saved = false;
                AdaptButtonToState();
            }
        }

        private void StarButton_Click(object sender, RoutedEventArgs e)
        {
            if (Photo.KernelShape != LensBlurPredefinedKernelShape.Star)
            {
                Photo.KernelShape = LensBlurPredefinedKernelShape.Star;
                AttemptUpdatePreviewAsync();
                Photo.Saved = false;
                AdaptButtonToState();
            }
        }

        private void HeartButton_Click(object sender, RoutedEventArgs e)
        {
            if (Photo.KernelShape != LensBlurPredefinedKernelShape.Heart)
            {
                Photo.KernelShape = LensBlurPredefinedKernelShape.Heart;
                AttemptUpdatePreviewAsync();
                Photo.Saved = false;
                AdaptButtonToState();
            }
        }

        private void OnSaveClicked(object sender, EventArgs e)
        {
            AttemptSaveAsync();
        }

        private async void AttemptSaveAsync()
        {
            if (!Processing)
            {
                Processing = true;

                AdaptButtonToState();

                GC.Collect();

                var lowMemory = false;

                try
                {
                    long result = (long)DeviceExtendedProperties.GetValue("ApplicationWorkingSetLimit");

                    lowMemory = result / 1024 / 1024 < 300;
                }
                catch (ArgumentOutOfRangeException)
                {
                }

                IBuffer buffer = null;

                Photo.OriginalImage.Position = 0;

                using (var source = new StreamImageSource(Photo.OriginalImage))
                using (var segmenter = new InteractiveForegroundSegmenter(source))
                using (var annotationsSource = new BitmapImageSource(Photo.AnnotationsBitmap))
                {
                    segmenter.Quality = lowMemory ? 0.5 : 1;
                    segmenter.AnnotationsSource = annotationsSource;

                    var foregroundColor = Photo.ForegroundBrush.Color;
                    var backgroundColor = Photo.BackgroundBrush.Color;

                    segmenter.ForegroundColor = Windows.UI.Color.FromArgb(foregroundColor.A, foregroundColor.R, foregroundColor.G, foregroundColor.B);
                    segmenter.BackgroundColor = Windows.UI.Color.FromArgb(backgroundColor.A, backgroundColor.R, backgroundColor.G, backgroundColor.B);

                    using (var effect = new LensBlurEffect(source, new LensBlurPredefinedKernel(Photo.KernelShape, (uint)Photo.KernelSize)))
                    using (var renderer = new JpegRenderer(effect))
                    {
                        effect.KernelMap = segmenter;

                        try
                        {
                            buffer = await renderer.RenderAsync();
                        }
                        catch (Exception ex)
                        {
                            System.Diagnostics.Debug.WriteLine("AttemptSave rendering failed: " + ex.Message);
                        }
                    }
                }

                if (buffer != null)
                {
                    using (var library = new MediaLibrary())
                    using (var stream = buffer.AsStream())
                    {
                        library.SavePicture("imageshot_" + DateTime.Now.Ticks, stream);

                        Photo.Saved = true;
                        MessageBox.Show("Your photo has been saved");
                        NavigationService.Navigate(new Uri("/MainPage.xaml", UriKind.Relative));
                    }
                }

                Processing = false;

                AdaptButtonToState();
            }
        }
    }
}