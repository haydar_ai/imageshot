﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using ImageShot.Resources;
using Microsoft.Phone.Tasks;
using ImageShot.Model;
using Microsoft.Devices;
using Nokia.Graphics.Imaging;

namespace ImageShot
{
    public partial class MainPage : PhoneApplicationPage
    {
        private CameraCaptureTask _cctask = new CameraCaptureTask();
        private PhotoChooserTask _task = new PhotoChooserTask();
        private PhotoResult _photoResult;

        // Constructor
        public MainPage()
        {
            InitializeComponent();

            // Sample code to localize the ApplicationBar
            //BuildLocalizedApplicationBar();
        }

        private void OnLensBlurClicked(object sender, System.Windows.Input.GestureEventArgs e)
        {
            _task.Show();
            _task.Completed += PhotoChooserTask_Completed;
        }

        private void PhotoChooserTask_Completed(object sender, PhotoResult e)
        {
            if (e.TaskResult == TaskResult.OK)
            {
                _photoResult = e;
                
            }
        }

        private void OnForegroundSegmentorClicked(object sender, System.Windows.Input.GestureEventArgs e)
        {
            _task.Show();
            _task.Completed += PhotoChooserTask_Completed;
        }

        private void OnTakePictureClicked(object sender, System.Windows.Input.GestureEventArgs e)
        {
            _cctask.Show();
            
            _cctask.Completed += PhotoChooserTask_Completed;
        }

        protected async override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            if (_photoResult != null)
            {
                Photo.OriginalImage = _photoResult.ChosenPhoto;
                Photo.Saved = false;

                _photoResult = null;

                NavigationService.Navigate(new Uri("/View/LensBlurSegmenterPage.xaml", UriKind.Relative));
            }
        }

        private void onClickAbout(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/View/AboutPage.xaml", UriKind.Relative));
        }

        // Sample code for building a localized ApplicationBar
        //private void BuildLocalizedApplicationBar()
        //{
        //    // Set the page's ApplicationBar to a new instance of ApplicationBar.
        //    ApplicationBar = new ApplicationBar();

        //    // Create a new button and set the text value to the localized string from AppResources.
        //    ApplicationBarIconButton appBarButton = new ApplicationBarIconButton(new Uri("/Assets/AppBar/appbar.add.rest.png", UriKind.Relative));
        //    appBarButton.Text = AppResources.AppBarButtonText;
        //    ApplicationBar.Buttons.Add(appBarButton);

        //    // Create a new menu item with the localized string from AppResources.
        //    ApplicationBarMenuItem appBarMenuItem = new ApplicationBarMenuItem(AppResources.AppBarMenuItemText);
        //    ApplicationBar.MenuItems.Add(appBarMenuItem);
        //}
    }
}